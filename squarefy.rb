#encoding: UTF-8

require 'rmagick'
require 'optparse'


# PARSE COMMAND-LINE OPTIONS
# ------------------------------------------------------------------------

options = {
    :colour => '#65656544',
    :width => 150,
    :height => 150,
    :blur => 5
}
OptionParser.new do |opts|
    opts.banner = "Usage: example.rb [options]"

    opts.on("-i", "--input=INPUTFILE", "Path to the original picture file") do |content|
        options[:input] = content
    end

    opts.on("-o" "--output=OUTPUTFILE", "Path and name to the resulting picture file") do |content|
        options[:output] = content
    end

    opts.on("-W", "--width=150", "Width of the resulting squares") do |content|
        options[:width] = content.to_i
    end

    opts.on("-H", "--height=150", "Height of the resulting squares") do |content|
        options[:height] = content.to_i
    end

    opts.on("-c", "--colour=#65656544", "Colour (with or without alpha) of the resulting squares") do |content|
        options[:colour] = "#" + content.upcase
    end

    opts.on("-b", "--blur=BLURLEVEL", "Level of the gaussian-blur filter") do |content|
        options[:blur] = content.to_i
    end
end.parse!

# CHECK GIVEN ARGUMENTS
# -----------------------------------------------------------------------
# Check that input file is present and exists
# Check given colour is consistent
# If no output file was given, generate a name.

if !options.has_key?(:input)
    abort "At last, need input file."
end

if !options.has_key?(":output")
    options[:output] = "SQ_" + File.basename(options[:input])
    puts "WARNING: No 'output file' specified, resulting picture will be saved at '#{options[:output]}'."
end

if !File.exists?(options[:input])
    abort "ERROR: Invalid input file."
end

if options[:colour].length <= 7
    options[:colour] = options[:colour] + "44"
    puts "WARNING: Given colour without alpha layer, added 44."
end

if (options[:colour] =~ /^#[0-9A-F]{8}$/).nil?
    abort "ERROR: Invalid given colour (it must be an HTML tag without # and with or without alpha layer)."
end

# RUN THE MAIN PROGRAM
# ----------------------------------------------------------------------

img = Magick::Image::read(options[:input])[0]

nnx = img.columns/options[:width]
nny = img.rows/options[:height]

pdx = (img.columns - nnx * options[:width]) / 2
pdy = (img.rows - nny * options[:height]) / 2

puts "INFO: Original picture is #{img.columns}x#{img.rows} pixels"
puts "INFO: #{nnx}x#{nny} squares of #{options[:width]}x#{options[:height]} will be drawn"

(pdx..(nnx * options[:width])).step(options[:width]) do |nx|
    (pdy..(nny * options[:height])).step(options[:height]) do |ny|
        rect = Magick::Draw.new
        if rand(2) == 1
            sub  = img.dispatch(nx, ny, options[:width], options[:height], "RGB")
            new_img = Magick::Image.constitute(options[:width], options[:height], "RGB", sub)
            img  = img.composite(new_img.gaussian_blur(0, options[:blur]), nx, ny, Magick::OverCompositeOp)

            #rect.stroke('red').stroke_width(2)
            rect.fill(options[:colour])
        else
            rect.fill('none')
        end
        
        rect.rectangle(nx, ny, nx + options[:width], ny + options[:height])
        rect.draw(img)
    end
end

img.write(options[:output])

