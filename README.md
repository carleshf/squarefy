# squarefy

## Introduction

Ruby script developed with the aim to create a _pixelized_ effect on pictures. This was done by creating square regions on the picture that add a gaussian-blur effect at the same time that tint the original picture.

## Dependences

 * `rmagick`
 * `optparse`

## Usage

```
% ruby squarefy.rb --help
Usage: example.rb [options]
     -i, --input=INPUTFILE            Path to the original picture file
     -o, --output=OUTPUTFILE          Path and name to the resulting picture file
     -W, --width=150                  Width of the resulting squares
     -H, --height=150                 Height of the resulting squares
     -c, --colour=#65656544           Colour (with or without alpha) of the resulting squares
     -b, --blur=BLURLEVEL             Level of the gaussian-blur filter
```

## Examples

### Example 1

By default the scripts addes squares of 150x150 pixels. This squares performs a blur effect of 5 and tints the region in black:

```
% ruby squarefy.rb  -i anne-hathaway-face.jpg -W 50 -H 50
```

The original picture is the following:

<p style="text-align:center;">
<img src="https://www.dropbox.com/s/vg7jkjbc8wvp53t/squarefy_anne-hathaway-face.jpg?dl=1" width="75%" height="75%" />
</p>

The script drawn a centered grid of 50x50 pixels square on the picture. Randomly it added a blur and tint effect to each square. The result follow:

<p style="text-align:center;">
<img src="https://www.dropbox.com/s/712en15aiq6j8s4/squarefy_SQ_anne-hathaway-face.jpg?dl=1" width="75%" height="75%" />
</p>

### Example 2

The following creates a grid using squares of 75x75 pixels. To the random squares applies a blur efect of 3 and a tint in blue:

```
% ruby squarefy.rb -i hugh-lauries-face.jpg -W 75 -H 75 -b 3
```

The original picture is the following:

<p style="text-align:center;">
<img src="https://www.dropbox.com/s/xs6bpozgd86yd91/squarefy_hugh-lauries-face.jpg?dl=1" width="75%" height="75%" />
</p>

The result is a grid with a really low level of blur and an electric blue colour:

<p style="text-align:center;">
<img src="https://www.dropbox.com/s/uhelafvspq51g8m/squarefy_SQ_hugh-lauries-face.jpg?dl=1" width="75%" height="75%" />
</p>
